## [v7.0.0-alpha3](https://gitlab.com/openflexure/openflexure-microscope/compare/v7.0.0-alpha2..v7.0.0-alpha3/) (2022-11-11)

* [!293](https://gitlab.com/openflexure/openflexure-microscope/-/merge_requests/293): Draft: experimental light trap
* [!307](https://gitlab.com/openflexure/openflexure-microscope/-/merge_requests/307): Restore Logitech C270 optics modules and add instructions for 35mm parfocal
* [!306](https://gitlab.com/openflexure/openflexure-microscope/-/merge_requests/306): Instructions improvements
* [!305](https://gitlab.com/openflexure/openflexure-microscope/-/merge_requests/305): Improve sangaboard workaround
* [!298](https://gitlab.com/openflexure/openflexure-microscope/-/merge_requests/298): FL cube - add holes for easier removal of filters
* [!304](https://gitlab.com/openflexure/openflexure-microscope/-/merge_requests/304): Documentation fixes
* [!303](https://gitlab.com/openflexure/openflexure-microscope/-/merge_requests/303): Improve string quoting on windows builds
* [!259](https://gitlab.com/openflexure/openflexure-microscope/-/merge_requests/259): Fix for build on Windows (double-quotes in OpenSCAD build commands are stripped out by cmd)
* [!302](https://gitlab.com/openflexure/openflexure-microscope/-/merge_requests/302): Improve bridging and aesthetics in cable tidies
* [!301](https://gitlab.com/openflexure/openflexure-microscope/-/merge_requests/301): Flip the nut entry slot on the condenser
* [!299](https://gitlab.com/openflexure/openflexure-microscope/-/merge_requests/299): Add renders of the sample clip assembly, and of the whole microscope
* [!300](https://gitlab.com/openflexure/openflexure-microscope/-/merge_requests/300): squashing some todos (Richard and Julian pair programming)
* [!257](https://gitlab.com/openflexure/openflexure-microscope/-/merge_requests/257): Mounting for Illumination board
* [!297](https://gitlab.com/openflexure/openflexure-microscope/-/merge_requests/297): Create zip of microscope source files and link to it in docs
* [!292](https://gitlab.com/openflexure/openflexure-microscope/-/merge_requests/292): Refactor optics module to implement changes from OFEP2
* [!289](https://gitlab.com/openflexure/openflexure-microscope/-/merge_requests/289): Simplify build script, removes STL selector data.
* [!295](https://gitlab.com/openflexure/openflexure-microscope/-/merge_requests/295): Fix regression which breaks illumination dovetail
* [!296](https://gitlab.com/openflexure/openflexure-microscope/-/merge_requests/296): nano position in nano_converter_plate.scad
* [!291](https://gitlab.com/openflexure/openflexure-microscope/-/merge_requests/291): Assorted fixes to instructions
* [!290](https://gitlab.com/openflexure/openflexure-microscope/-/merge_requests/290): Objective mount improvements
* [!284](https://gitlab.com/openflexure/openflexure-microscope/-/merge_requests/284): Added screws to retain small gear
* [!287](https://gitlab.com/openflexure/openflexure-microscope/-/merge_requests/287): Smoothed feet and fixed typo
* [!286](https://gitlab.com/openflexure/openflexure-microscope/-/merge_requests/286): Update okh.yml
* [!288](https://gitlab.com/openflexure/openflexure-microscope/-/merge_requests/288): Fix build which was broken by a reorganisation of STL extras repository
* [!285](https://gitlab.com/openflexure/openflexure-microscope/-/merge_requests/285): Update instructions to take advantage of GitBuilding v0.11
* [!283](https://gitlab.com/openflexure/openflexure-microscope/-/merge_requests/283): Prevent zenodo CI job running on "web"

## [v7.0.0-alpha2](https://gitlab.com/openflexure/openflexure-microscope/compare/v7.0.0-alpha1..v7.0.0-alpha2/) (2022-01-21)

* [!281](https://gitlab.com/openflexure/openflexure-microscope/-/merge_requests/281): Pi 3 and Sangaboard v0.3 support in electronics tray
* [!282](https://gitlab.com/openflexure/openflexure-microscope/-/merge_requests/282): Implementing GitBuilding zip links to download a zip of the STLs needed
* [!280](https://gitlab.com/openflexure/openflexure-microscope/-/merge_requests/280): Document LED wiring
* [!279](https://gitlab.com/openflexure/openflexure-microscope/-/merge_requests/279): Cutout for the illumination cable
* [!277](https://gitlab.com/openflexure/openflexure-microscope/-/merge_requests/277): Minor code cleanup
* [!274](https://gitlab.com/openflexure/openflexure-microscope/-/merge_requests/274): Improve docs
* [!276](https://gitlab.com/openflexure/openflexure-microscope/-/merge_requests/276): Precompiling main body stl for renders
* [!272](https://gitlab.com/openflexure/openflexure-microscope/-/merge_requests/272): Merge upright microscope into master
* [!260](https://gitlab.com/openflexure/openflexure-microscope/-/merge_requests/260): Code and structure clean up of the parts for upright microscope
* [!264](https://gitlab.com/openflexure/openflexure-microscope/-/merge_requests/264): Fix stl meshes
* [!248](https://gitlab.com/openflexure/openflexure-microscope/-/merge_requests/248): Faster render
* [!246](https://gitlab.com/openflexure/openflexure-microscope/-/merge_requests/246): Updates to docs that require GitBuilding v0.9 to be released
* [!265](https://gitlab.com/openflexure/openflexure-microscope/-/merge_requests/265): Include stl_options.json in build output again
* [!263](https://gitlab.com/openflexure/openflexure-microscope/-/merge_requests/263): Add grant ids to zenodo metadata
* [!262](https://gitlab.com/openflexure/openflexure-microscope/-/merge_requests/262): Validate STLs
* [!261](https://gitlab.com/openflexure/openflexure-microscope/-/merge_requests/261): Fix compile error with OpenSCAD nightly build.
* [!258](https://gitlab.com/openflexure/openflexure-microscope/-/merge_requests/258): Explictly defining self tapping holes.
* [!256](https://gitlab.com/openflexure/openflexure-microscope/-/merge_requests/256): Upright instructions
* [!253](https://gitlab.com/openflexure/openflexure-microscope/-/merge_requests/253): Upright

## [v7.0.0-alpha1](https://gitlab.com/openflexure/openflexure-microscope/compare/v6.1.4..v7.0.0-alpha1/) (2021-08-19)

* [!242](https://gitlab.com/openflexure/openflexure-microscope/-/merge_requests/242): Improve stand
* [!235](https://gitlab.com/openflexure/openflexure-microscope/-/merge_requests/235): Allow passing arguments to ninja again
* [!255](https://gitlab.com/openflexure/openflexure-microscope/-/merge_requests/255): Cable tidy caps
* [!247](https://gitlab.com/openflexure/openflexure-microscope/-/merge_requests/247): Run pylint on build system
* [!254](https://gitlab.com/openflexure/openflexure-microscope/-/merge_requests/254): Added lots of comments
* [!252](https://gitlab.com/openflexure/openflexure-microscope/-/merge_requests/252): Mainbody nut trap
* [!245](https://gitlab.com/openflexure/openflexure-microscope/-/merge_requests/245): Move to OpenSCAD 2021 installed via a .deb file
* [!236](https://gitlab.com/openflexure/openflexure-microscope/-/merge_requests/236): Only warn about unclean dir if --force-clean is used
* [!243](https://gitlab.com/openflexure/openflexure-microscope/-/merge_requests/243): Mention oiling the actuator in the docs
* [!229](https://gitlab.com/openflexure/openflexure-microscope/-/merge_requests/229): Use gitbuilding
* [!241](https://gitlab.com/openflexure/openflexure-microscope/-/merge_requests/241): Fix outer thread generation code. This was a bug from cleanup
* [!239](https://gitlab.com/openflexure/openflexure-microscope/-/merge_requests/239): Rendering the picamera cover during assembly, flipping the condenser lens
* [!238](https://gitlab.com/openflexure/openflexure-microscope/-/merge_requests/238): Adjust camera cover around screws and for connector space
* [!237](https://gitlab.com/openflexure/openflexure-microscope/-/merge_requests/237): Fix dictionary errors in OpenSCAD 2021
* [!231](https://gitlab.com/openflexure/openflexure-microscope/-/merge_requests/231): Scad cleanup March 2021
* [!232](https://gitlab.com/openflexure/openflexure-microscope/-/merge_requests/232): Fix subprocess calls to get return codes in render.py
* [!234](https://gitlab.com/openflexure/openflexure-microscope/-/merge_requests/234): Move microscope stand functions to library
* [!233](https://gitlab.com/openflexure/openflexure-microscope/-/merge_requests/233): Add SCA2D code quality to CI and artifact to gitignore
* [!223](https://gitlab.com/openflexure/openflexure-microscope/-/merge_requests/223): Base for microscope with cable managment
* [!222](https://gitlab.com/openflexure/openflexure-microscope/-/merge_requests/222): Added illumination thumbscrew for new illumination
* [!221](https://gitlab.com/openflexure/openflexure-microscope/-/merge_requests/221): Improve version string
* [!230](https://gitlab.com/openflexure/openflexure-microscope/-/merge_requests/230): Modify counterbores and screw holes in picam2_cutout picamera_2_camera_mount and lens_spacer
* [!225](https://gitlab.com/openflexure/openflexure-microscope/-/merge_requests/225): Include rendering in build system
* [!216](https://gitlab.com/openflexure/openflexure-microscope/-/merge_requests/216): New illumination dovetail
* [!214](https://gitlab.com/openflexure/openflexure-microscope/-/merge_requests/214): Cable management
* [!227](https://gitlab.com/openflexure/openflexure-microscope/-/merge_requests/227): Allow generating json without including extra files
* [!224](https://gitlab.com/openflexure/openflexure-microscope/-/merge_requests/224): Fix uneven flexure length on back legs
* [!228](https://gitlab.com/openflexure/openflexure-microscope/-/merge_requests/228): Broke STL selector options into parseable functions
* [!212](https://gitlab.com/openflexure/openflexure-microscope/-/merge_requests/212): More renders for the documentation
* [!211](https://gitlab.com/openflexure/openflexure-microscope/-/merge_requests/211): Remove uneeded hole on sides of feet.
* [!210](https://gitlab.com/openflexure/openflexure-microscope/-/merge_requests/210): Assembly renders
* [!209](https://gitlab.com/openflexure/openflexure-microscope/-/merge_requests/209): Make render fail on errors
* [!208](https://gitlab.com/openflexure/openflexure-microscope/-/merge_requests/208): Add select_stl_if="always"
* [!206](https://gitlab.com/openflexure/openflexure-microscope/-/merge_requests/206): Reflection cherrypicks
* [!189](https://gitlab.com/openflexure/openflexure-microscope/-/merge_requests/189): Led array holder
* [!205](https://gitlab.com/openflexure/openflexure-microscope/-/merge_requests/205): Reorganise into libraries and tidying up dependencies
* [!204](https://gitlab.com/openflexure/openflexure-microscope/-/merge_requests/204): Introduce scad dictionaries
* [!203](https://gitlab.com/openflexure/openflexure-microscope/-/merge_requests/203): Illumination cleanup
* [!202](https://gitlab.com/openflexure/openflexure-microscope/-/merge_requests/202): Honour the endstops flag
* [!181](https://gitlab.com/openflexure/openflexure-microscope/-/merge_requests/181): Med stable
* [!199](https://gitlab.com/openflexure/openflexure-microscope/-/merge_requests/199): Refactor build system to make the build.py useful again
* [!196](https://gitlab.com/openflexure/openflexure-microscope/-/merge_requests/196): Document size of dichroic filter slot (from SCAD)
* [!192](https://gitlab.com/openflexure/openflexure-microscope/-/merge_requests/192): Draft: Refactoring to try to move away from too many dangerous includes
* [!194](https://gitlab.com/openflexure/openflexure-microscope/-/merge_requests/194): Removed duplicated utility modules.
* [!193](https://gitlab.com/openflexure/openflexure-microscope/-/merge_requests/193): Improved central_optics_cut_out.
* [!190](https://gitlab.com/openflexure/openflexure-microscope/-/merge_requests/190): Split xy positioning system into separate module.
* [!188](https://gitlab.com/openflexure/openflexure-microscope/-/merge_requests/188): Fixes over-enthusiastic cutout in xy-base.
* [!183](https://gitlab.com/openflexure/openflexure-microscope/-/merge_requests/183): Separated STL generate and copy from Writer functionality
* [!182](https://gitlab.com/openflexure/openflexure-microscope/-/merge_requests/182): Reducing the number of options built as standard
* [!178](https://gitlab.com/openflexure/openflexure-microscope/-/merge_requests/178): Remove need for sample riser by adding nut traps to taller XY stage
* [!180](https://gitlab.com/openflexure/openflexure-microscope/-/merge_requests/180): Fix STL selection for picamera platform and cover
* [!179](https://gitlab.com/openflexure/openflexure-microscope/-/merge_requests/179): Link to microscope-stls.openflexure.org
* [!176](https://gitlab.com/openflexure/openflexure-microscope/-/merge_requests/176): Re-creating just_leg_test
* [!177](https://gitlab.com/openflexure/openflexure-microscope/-/merge_requests/177): Tidied up accessories
* [!175](https://gitlab.com/openflexure/openflexure-microscope/-/merge_requests/175): LFS and README changes

## [v6.1.4](https://gitlab.com/openflexure/openflexure-microscope/compare/v6.1.3..v6.1.4/) (2020-11-02)

* [!174](https://gitlab.com/openflexure/openflexure-microscope/-/merge_requests/174): Sample riser changed: Elongated counterbore and trylobular holes for clips
* [!155](https://gitlab.com/openflexure/openflexure-microscope/-/merge_requests/155): Add JSON output of STL options for the JS web stl selector
* [!168](https://gitlab.com/openflexure/openflexure-microscope/-/merge_requests/168): Include prebuilt stl files
* [!163](https://gitlab.com/openflexure/openflexure-microscope/-/merge_requests/163): Json build output stand height

## [v6.1.3](https://gitlab.com/openflexure/openflexure-microscope/compare/v6.1.2..v6.1.3/) (2020-10-12)

* [!167](https://gitlab.com/openflexure/openflexure-microscope/-/merge_requests/167): Fix: Corrects small typo in 0_bill_of_materials.md
* [!170](https://gitlab.com/openflexure/openflexure-microscope/-/merge_requests/170): Added letters to the feet.
* [!171](https://gitlab.com/openflexure/openflexure-microscope/-/merge_requests/171): Added OF emblem to illumination dovetail
* [!169](https://gitlab.com/openflexure/openflexure-microscope/-/merge_requests/169): Add a tip about power supply.
* [!166](https://gitlab.com/openflexure/openflexure-microscope/-/merge_requests/166): Print page clarity and circular link.
* [!162](https://gitlab.com/openflexure/openflexure-microscope/-/merge_requests/162): Wall thickness and nano-based motor driver case
* [!164](https://gitlab.com/openflexure/openflexure-microscope/-/merge_requests/164): Fixed fl cutout
* [!152](https://gitlab.com/openflexure/openflexure-microscope/-/merge_requests/152): Zenodo integration
* [!165](https://gitlab.com/openflexure/openflexure-microscope/-/merge_requests/165): Adding the motor order to the documentation
* [!157](https://gitlab.com/openflexure/openflexure-microscope/-/merge_requests/157): Updated microscope_stand for parametric wall thickness
* [!160](https://gitlab.com/openflexure/openflexure-microscope/-/merge_requests/160): Added a taller microscope stand
* [!158](https://gitlab.com/openflexure/openflexure-microscope/-/merge_requests/158): Make build work out of the box on MacOS
* [!159](https://gitlab.com/openflexure/openflexure-microscope/-/merge_requests/159): Add actuator tension band printable with TPU filament

## [v6.1.2](https://gitlab.com/openflexure/openflexure-microscope/compare/v6.1.1..v6.1.2/) (2020-05-20)

* [!150](https://gitlab.com/openflexure/openflexure-microscope/-/merge_requests/150): Fixing the hole spacing for the sangaboard
* [!156](https://gitlab.com/openflexure/openflexure-microscope/-/merge_requests/156): Build instructions update
* [!154](https://gitlab.com/openflexure/openflexure-microscope/-/merge_requests/154): Fixed offset causing tools to attempt to print at different heights
* [!153](https://gitlab.com/openflexure/openflexure-microscope/-/merge_requests/153): Fixes for travel case
* [!149](https://gitlab.com/openflexure/openflexure-microscope/-/merge_requests/149): Made motor lugs disable-able

## [v6.1.1](https://gitlab.com/openflexure/openflexure-microscope/compare/v6.1.0..v6.1.1/) (2020-02-25)

* [!147](https://gitlab.com/openflexure/openflexure-microscope/-/merge_requests/147): Smarter brim and better slide riser
* [!148](https://gitlab.com/openflexure/openflexure-microscope/-/merge_requests/148): Tool holder for band insertion tool
* [!146](https://gitlab.com/openflexure/openflexure-microscope/-/merge_requests/146): Tidying up explanation of Git LFS as Joe and I got confused again
* [!145](https://gitlab.com/openflexure/openflexure-microscope/-/merge_requests/145): split out top of z axis for easier integration into other assemblies

## [v6.1.0](https://gitlab.com/openflexure/openflexure-microscope/compare/v6.0.0..v6.1.0/) (2020-01-20)

* [!144](https://gitlab.com/openflexure/openflexure-microscope/-/merge_requests/144): Fixed link to kitspace for sangaboard v0.3
* [!143](https://gitlab.com/openflexure/openflexure-microscope/-/merge_requests/143): Move mounting hole out of the way of sd card cutout
* [!142](https://gitlab.com/openflexure/openflexure-microscope/-/merge_requests/142): Updated repositories in README
* [!134](https://gitlab.com/openflexure/openflexure-microscope/-/merge_requests/134): Make a bigger opening for pi sd card
* [!141](https://gitlab.com/openflexure/openflexure-microscope/-/merge_requests/141): Refactor openscad parameter generation in build py
* [!139](https://gitlab.com/openflexure/openflexure-microscope/-/merge_requests/139): Pi4 compatible base
* [!140](https://gitlab.com/openflexure/openflexure-microscope/-/merge_requests/140): Fixed broken links in documentation

## [v6.0.0](https://gitlab.com/openflexure/openflexure-microscope/compare/v5.20.0-b..v6.0.0/) (2019-10-17)

* [!137](https://gitlab.com/openflexure/openflexure-microscope/-/merge_requests/137): Reflection docs
* [!136](https://gitlab.com/openflexure/openflexure-microscope/-/merge_requests/136): Updated better reflection illuminator
* [!131](https://gitlab.com/openflexure/openflexure-microscope/-/merge_requests/131): Ninja build system
* [!135](https://gitlab.com/openflexure/openflexure-microscope/-/merge_requests/135): 6.0.0-beta.1
* [!133](https://gitlab.com/openflexure/openflexure-microscope/-/merge_requests/133): Fix generate_makefile documentation
* [!125](https://gitlab.com/openflexure/openflexure-microscope/-/merge_requests/125): First version of stronger feet
* [!132](https://gitlab.com/openflexure/openflexure-microscope/-/merge_requests/132): Thumbwheels for easier manual control
* [!130](https://gitlab.com/openflexure/openflexure-microscope/-/merge_requests/130): Refactor printing instructions
* [!129](https://gitlab.com/openflexure/openflexure-microscope/-/merge_requests/129): Added links to further information on optics components
* [!128](https://gitlab.com/openflexure/openflexure-microscope/-/merge_requests/128): Add type of condenser lens
* [!124](https://gitlab.com/openflexure/openflexure-microscope/-/merge_requests/124): Make mounting holes fit Sangaboard v0.3
* [!123](https://gitlab.com/openflexure/openflexure-microscope/-/merge_requests/123): Reduce LED size so it's gripped properly
* [!115](https://gitlab.com/openflexure/openflexure-microscope/-/merge_requests/115): Add .gitlab-ci.yml for autobuilding STLs

## [v5.20.0-b](https://gitlab.com/openflexure/openflexure-microscope/compare/v5.16.10-beta..v5.20.0-b/) (2019-01-21)


## [v5.16.10-beta](https://gitlab.com/openflexure/openflexure-microscope/compare/v5.15.2..v5.16.10-beta/) (2017-03-24)


## [v5.15.2](https://gitlab.com/openflexure/openflexure-microscope/compare/v5.15.2-rc1..v5.15.2/) (2016-06-22)


## [v5.15.2-rc1](https://gitlab.com/openflexure/openflexure-microscope/compare/v5.15.2a..v5.15.2-rc1/) (2016-06-08)


## [v5.15.2a](https://gitlab.com/openflexure/openflexure-microscope/compare/v5.15.1-rc0..v5.15.2a/) (2017-02-27)


## [v5.15.1-rc0](https://gitlab.com/openflexure/openflexure-microscope/compare/None..v5.15.1-rc0/) (2016-05-31)


