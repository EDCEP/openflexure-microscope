* [Illumination dovetail]{output,qty:1}: [illumination_dovetail.stl](models/illumination_dovetail.stl){previewpage}
* [Condenser arm]{output,qty:1}: [condenser.stl](models/condenser.stl){previewpage}
* [Condenser board spacer]{output,qty:1}: [condenser_board_spacer.stl](models/condenser_board_spacer.stl){previewpage}
* [Condenser lid]{output,qty:1}: [condenser_lid.stl](models/condenser_lid.stl){previewpage}
* [Illumination thumbscrew]{output,qty:1}: [illumination_thumbscrew.stl](models/illumination_thumbscrew.stl){previewpage}
* 3 [large gears]{output,qty:3}: [large_gears.stl](models/large_gears.stl){previewpage}
* 3 [feet]{output,qty:3}: [feet.stl](models/feet.stl){previewpage}