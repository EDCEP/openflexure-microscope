# Mount the optics and the microscope


{{BOM}}

[2.5mm Ball-end Allen key]: parts/tools/2.5mmBallEndAllenKey.md

## Mount the optics {pagestep}

![](renders/mount_optics_{{var_optics, default:rms}}1.png)
![](renders/mount_optics_{{var_optics, default:rms}}2.png)
![](renders/mount_optics_{{var_optics, default:rms}}3.png)
![](renders/mount_optics_{{var_optics, default:rms}}4.png)

* Get the [2.5mm Ball-end Allen key]{qty:1, cat:tool} ready
* Take the [complete optics module](fromstep){qty:1, cat:subassembly} and pass it through the bottom of the [microscope][microscope with assembled actuators](fromstep){qty:1, cat:subassembly}.
* Insert exposed the mounting screw on the optics module through the keyhole on the z-actuator.

![](renders/mount_optics_{{var_optics, default:rms}}5.png)
![](renders/mount_optics_{{var_optics, default:rms}}6.png)
![](renders/mount_optics_{{var_optics, default:rms}}7.png)
![](renders/mount_optics_{{var_optics, default:rms}}8.png)

* Insert the Allen key through the teardrop shaped hole on the front of the microscope. Until it engages with the mounting screw.
* Slide optics module up the keyhole as high as it will go.
* Tighten the screw with the Allen key to lock the optics in place.

{{include: mount_microscope.md}}
