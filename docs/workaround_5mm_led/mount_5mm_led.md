# Mount the 5mm LED

If you don't have a PCB for the illumination, you can mount a 5mm LED instead.  

>i The 5mm LED mounts to the lid of the condenser, not in the same place as the PCB.

{{BOM}}

[No 2 6.5mm self tapping screws]: ../parts/mechanical.yml#SelfTap_PoziPan_No2x6.5_SS "{cat:mech}"
[#1 pozidrive screwdriver]: ../parts/tools/pozidrive_1_screwdriver.md "{cat:tool}"
[LED assembly]: fromstep "{cat:electronic}"
[condenser LED holder]: fromstep "{cat:printedPart}"

## Mount the LED {pagestep}

![Exploded view of the LED and LED holder](../renders/workaround_5mm_led1.png)
![LED in the LED holder, above the condenser lid](../renders/workaround_5mm_led2.png)
![LED in the condenser lid, ready for screws](../renders/workaround_5mm_led3.png)
![Exploded view of the mounting screws](../renders/workaround_5mm_led4.png)
![The assembled LED and condenser lid](../renders/workaround_5mm_led5.png)

* Push-fit the [LED assembly]{qty:1} into the [condenser LED holder]{qty:1} printed in the first step.
* Secure the LED holder onto the lid of the condenser with two [No 2 6.5mm self tapping screws]{qty:2} using a [#1 pozidrive screwdriver]{qty:1}.

## Mount the diffuser {pagestep}

* Fix the diffuser [i](../info_pages/illumination_optics_explanation.md) onto the condenser, as described in the main condenser page, omitting the board spacer and PCB.
* Alternatively, PTFE tape can be used on the LED, as in previous versions of the instructions, instead of the diffuser.